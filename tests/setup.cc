//-----------------------------------------------------------
//
//    Copyright (C) 2014 by the deal.II authors
//
//    This file is subject to LGPL and may not be distributed
//    without copyright and license information. Please refer
//    to the file deal.II/doc/license.html for the  text  and
//    further information on this license.
//
//-----------------------------------------------------------

#include "tests.h"

#include "laplacian.h"

template <int dim>
void test()
{
  Laplacian<dim> laplacian;
  laplacian.generate_grid();
  laplacian.setup_system();

  deallog << "===============" << dim << std::endl
          << "Testing dim = " << dim << std::endl
          << "N cells: " << laplacian.triangulation.n_active_cells() << std::endl
          << "N dofs : " << laplacian.dof_handler.n_dofs() << std::endl;
}

int main ()
{
  initlog();

  test<1>();
  test<2>();
  test<3>();
}
