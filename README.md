# Simple deal.II app, with testing and CI on both linux and windows

[![Build Status](https://gitlab.com/MDLSoft/minimal-windows-test/badges/master/build.svg)](https://gitlab.com/MDLSoft/minimal-windows-test/commits/master) [![Build status](https://ci.appveyor.com/api/projects/status/61kecntnaxmnxfwj?svg=true)](https://ci.appveyor.com/project/luca-heltai/minimal-windows-test)

A bare deal.II application, with directory structure, and private
testsuite.

This repository can be used to bootstrap your own deal.II
application. The structure of the directory is the following:

	./source
	./include
	./tests

The directories contain a minimal working application (deal.II hello
world) which solves the Poisson problem using cg, in 1,2, and 3 dimensions.

The CMakeLists.txt will generate an executable for each dimension, and for each 
build type (Release and Debug) and a library for each build type
containing all cc files **except** source/main.cc. This library is
added to the running tests, so that you can make tests on your
application just as you would do with the deal.II library.

Modify the TARGET variable in the CMakeLists.txt to your application
name. Two libraries named ./tests/${TARGET}lib and ./tests/${TARGET}lib.g 
will be generated together with the executable when you compile your 
application.

After you have compiled your application, you can run 

	make test

or
	
	ctest 

to start the testsuite.

Take a look at
https://www.dealii.org/developer/developers/testsuite.html for more
information on how to create tests and add categories of tests.

Both `.travis.yml`, `.gitlab-ci.yml`, and `.appveyor.yml` files are provided that 
build the application and run the tests in the tests directory using
ctest, in continuous integration, by running under docker with the 
image provided on dockerhub.com: `dealii/dealii`, or on appveyor build system
for windows.

## Licence

Please see the file ./LICENSE for details
